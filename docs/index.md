---
hide:
  - navigation
  - toc
  - footer
---

# 欢迎

<style>
.left {
  vertical-align: top;
  display: inline-block;
  margin-top: 0px;
  margin-bottom: 0px;
  margin-right: 0px;
  margin-left: 0px;
}
.right {
  vertical-align: top;
  display: inline-block;
  margin-top: 0px;
  margin-bottom: 0px;
  margin-right: 0px;
  margin-left: 0px;
}
</style>

<div float=left style="width:59%" class=left>
    <img src="./art/images/former2.jpg">
    <img src="./art/images/romanita1-1024x647.jpg">
</div>
<div float=left style="width:39%" class=right>
    <img src="./art/images/former5-1024x825.jpg">
    <img src="./art/images/former3-1024x706.jpg">
    <img src="./art/images/banner.jpg">
</div>
