# 立体几何

## 体积

$V_{棱柱}=Sh$

$V_{棱锥}=\frac13Sh$

$V_{棱台}=\frac13h(S'+\sqrt{S'S}+S)$

$V_{圆柱}=\pi{r^2}h$

$V_{圆锥}=\frac13\pi{r^2}h$

$V_{圆台}=\frac13\pi{h}({r'}^2+r'r+r^2)$

$V_{球}=\frac43\pi{R^3}$

<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP3%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E7%AB%8B%E4%BD%93%E5%87%A0%E4%BD%95%2B%E7%A9%BA%E9%97%B4%E5%90%91%E9%87%8F%E7%B3%BB%E7%BB%9F%E6%A2%B3%E7%90%86.mp4?Authorization=3_20220703012353_02ba1d5fc8aad3998db72b10_ac2fff2a7b634a41d35e7695442d75ad7de5f6fb_004_20220710012353_0000_dnld" type="video/mp4">
</video>
<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP6%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E3%80%90%E8%A7%A3%E6%9E%90%E5%87%A0%E4%BD%95%2B%E5%AF%BC%E6%95%B0%E3%80%91%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%85%A8%E6%A2%B3%E7%90%86%EF%BC%81.%E5%B0%8F%E5%A7%9A%E8%80%81%E5%B8%88.mp4?Authorization=3_20220703021105_079396760100063016feb347_039462fcfcff88c7250401620450dc273847eeae_004_20220710021105_0000_dnld" type="video/mp4">
</video>
