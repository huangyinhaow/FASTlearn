# 不等式

## 重要不等式

$a^2+b^2≥2ab$

因为 $a, b\in \mathbf{R}, (a-b)^2≥0$，当且仅当 $a=b$ 时，等号成立，所以 $a^2+b^2-2ab≥0$

## 基本不等式

$\sqrt{ab}≤\frac{a+b}2$

<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP3%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E7%AB%8B%E4%BD%93%E5%87%A0%E4%BD%95%2B%E7%A9%BA%E9%97%B4%E5%90%91%E9%87%8F%E7%B3%BB%E7%BB%9F%E6%A2%B3%E7%90%86.mp4?Authorization=3_20220703020840_968626127aae2a25c9e51784_ece5b4ee122cbb8d0a59bc9721bece205d3ff058_004_20220710020840_0000_dnld" type="video/mp4">
</video>
