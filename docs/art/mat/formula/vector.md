# 平面向量

## 平面向量的线性计算

### 平面向量的加法

$\boldsymbol{a+0=0+a=a}$

$\boldsymbol{|a+b|≤|a|+|b|}$

$\boldsymbol{(a+b)+c=a+(b+c)}$

### 平面向量的减法

$\boldsymbol{-(-a)=a}$

$\boldsymbol{a+(-a)=(-a)+a=0}$

$\boldsymbol{a-b=a+(-b)}$

### 平面向量的数乘

引入：

$\boldsymbol{(-a)+(-a)+(-a)=-3a}$

$\boldsymbol{|-3a|=3|a|}$

根据定义得：

$\boldsymbol{\lambda(\mu a)=(\lambda \mu)a}$

$\boldsymbol{(\lambda+\mu)a=\lambda a+\lambda b}$

$\boldsymbol{\lambda(a+b)=\lambda a+\lambda b}$

$\boldsymbol{(-\lambda)a=-(\lambda a)=\lambda (-a)}$

$\boldsymbol{\lambda(a-b)=\lambda a-\lambda b}$

概括得：

$\boldsymbol{\lambda({\mu_1}a±{\mu_2}b)=\lambda {\mu_1} a±\lambda {\mu_2} b}$

!!! Example
    $\boldsymbol{(2a+3b-c)-(3a-2b+c)=2a+3b-c-3a+2b-c=-a+5b-2c}$

若 $\boldsymbol{a}$ 与 $\boldsymbol{b}$ 共线，则：

$b=\lambda a$

## 平面向量的数量积

引入：

$W=\boldsymbol{|F||s|\cos \theta}$

设 $\boldsymbol{a}$ 与 $\boldsymbol{b}$ 的夹角为 $\theta$ ，则：

$\boldsymbol{a·b=|a||b|\cos \theta}$

根据定义，得：

$\boldsymbol{a·b=b·a}$

$\boldsymbol{(\lambda a)·b=\lambda(a·b)=a·(\lambda b)}$

$\boldsymbol{(a+b)·c=a·c+b·c}$

## 平面向量的坐标表示

如果 $\boldsymbol{e_1}$ ， $\boldsymbol{e_2}$ 是同一平面内两个不共线向量那么对于这一平面内的任意向量 $\boldsymbol{a}$ ，有且只有一个实数 $\lambda_1$ ，$\lambda_2$ ，使：

$\boldsymbol{a=\lambda_1 e_1+\lambda_2 e_2}$

此时，{$\boldsymbol{e_1}$ ，$\boldsymbol{e_2}$} 是该平面所有向量的基底，任何一个向量都可以由同一个基底唯一表示，即 $\boldsymbol{a} = x\boldsymbol{e_1}+y\boldsymbol{e_2}$.

向量的和差的坐标等于其坐标的和差，即：

$\boldsymbol{a-b} = (x_1 - x_2, y_1 - y_2)$

向量与实数的积的坐标等于原向量与实数的积的坐标，即：

$\lambda \boldsymbol{a}=(\lambda x, \lambda y)$

两个向量的数量积等于它们对应坐标的乘积的和，即：

$\boldsymbol{a·b} = x_1 x_2 + y_1 y_2$

若 $\boldsymbol{a⊥b}$ ，则：

$\boldsymbol{a·b} = 0$

## 平面向量与三角形

引入：

$\boldsymbol{|F_1|=}\frac{\boldsymbol{|G|}}{2\cos \frac {\theta} 2}$

### 余弦定理

$a^2=b^2+c^2-2bc\cos A$

推论：

$\cos A = \frac {b^2+c^2-a^2} {2bc}$

### 正弦定理

$\frac a {\sin A}=\frac b {\sin B}=\frac c {\sin C}=2R$

其中，$R$ 是三角形外接圆的半径。

在 $△ABC$ 中， $A>B\Leftrightarrow\sin{A}>\sin{B}\Leftrightarrow{a>b}$

### 面积公式

$S=\sqrt{p(p-a)(p-b)(p-c)}, p=\frac 1 2 (a+b+c)$

$S=\frac{1}2ah_a=\frac{1}2bh_b=\frac{1}2ch_c$

$S=\frac{1}2ab\sin{C}=\frac{1}2bc\sin{A}=\frac{1}2ca\sin{B}$

<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP4%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E3%80%90%E4%B8%8D%E7%AD%89%E5%BC%8F%2B%E5%90%91%E9%87%8F%2B%E6%95%B0%E5%88%97%E3%80%91%E6%A0%B8%E5%BF%83%E6%A2%B3%E7%90%86.%E5%B0%8F%E5%A7%9A%E8%80%81%E5%B8%88.%E6%9C%80%E5%90%8E6%E8%AF%BE%E7%AC%AC4%E8%AF%BE.mp4?Authorization=3_20220703012515_f95a012b55c5b92a18e4bde2_542c8b6deeae76cd8e4ca5fb69da5c1613d2e3ae_004_20220710012515_0000_dnld" type="video/mp4">
</video>
