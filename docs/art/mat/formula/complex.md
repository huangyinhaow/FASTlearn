# 复数

$z=a+bi$（$a,b\in R$）

其中，$a$ 是实部，$b$ 是虚部

$i^1=i, i^2=-1, i^3=-i, i^4=1,i^5=-i, \cdots$

## 复数的模

$|z|=|a+bi|=\sqrt{a^2+b^2}$（$a,b\in R$）

## 复数的四则运算

### 复数的加减法

$z_1+z_2=z_2+_1$

$(z_1+z_2)+z_3=z_1+(z_2+z_3)$

$(a+bi)-(c+di)=(a-c)+(b-d)i$

### 复数的乘除法

$z_1 z_2=z_2 z_1$

$(z_1 z_2)z_3=z_1 (z_2 z_3)$

$z_1 (z_2 + z_3)=z_1 z_2+z_1 z_3$

$(a+bi)÷(c+bi)=\frac {ac+bd} {c^2+d^2}+\frac {bc-ad} {c^2+d^2}$

$\frac{z_1}{z_2}=\frac{z_1\cdotp\overline{z_2}}{|z_2|^2}$

$|\frac{z_1}{z_2}|=\frac{|z_1|}{|z_2|}$

### 常见运算

$\frac{1+i}{1-i}=i$

$\frac{1-i}{1+i}=-i$

$(a+bi)i=-b+ai$

$\frac{-b+ai}{a+bi}=i$

$(a-bi)i=b+ai$

$\frac{b+ai}{a-bi}=i$

## 复数的三角表示

任何一个复数 $z=a+bi$ 都可以表示成：

$r(\cos \theta +i\sin \theta)$

其中，$r$ 是 复数$z$ 的模
