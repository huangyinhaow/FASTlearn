# 三角函数

## 诱导公式

### 公式一

$\sin (a+k·2\pi)=\sin a$

$\cos (a+k·2\pi)=\cos a$

$\tan (a+k·2\pi)=\tan a$

同角三角函数的关系：

$\sin^2 a+\cos^2 a=1$

$\tan a=\frac {\sin a} {\cos a}$

$\frac {\cos x} {1-\sin x}=\frac {1+\sin x} {\cos x}$

### 公式二

$\sin (\pi +a)=-\sin a$

$\cos (\pi +a)=-\cos a$

$\tan (\pi +a)=\tan a$

### 公式三

$\sin (-a)=-\sin a$

$\cos (-a)=\cos a$

$\tan (-a)=-\tan a$

### 公式四

$\sin (\pi -a)=\sin a$

$\cos (\pi -a)=-\cos a$

$\tan(\pi -a)=-\tan a$

### 公式五

$\sin (\frac \pi 2 -a)=\cos a$

$\cos (\frac \pi 2 -a)=\sin a$

### 公式六

$\sin (\frac \pi 2 +a)=\cos a$

$\cos (\frac \pi 2 +a)=-\sin a$

## 拓展

$\sin(\frac{3\pi}2+\alpha)=-\cos\alpha$

$\cos(\frac{3\pi}2+\alpha)=\sin\alpha$

$\sin(\frac{3\pi}2-\alpha)=-\cos\alpha$

$\cos(\frac{3\pi}2-\alpha)=-\sin\alpha$

## 周期性

$f(x+T)=f(x)$

### 正弦函数

#### 周期

$2k\pi(k\in\mathbf{Z}, k≠0)$

#### 最小正周期

$2\pi$

### 余弦函数

#### 周期

$2k\pi(k\in\mathbf{Z}, k≠0)$

#### 最小正周期

$2\pi$

### 正切函数

#### 周期

$\pi$

## 单调性

### 正弦函数

#### 单调递增

$[-\frac\pi 2+2k\pi, \frac\pi 2 +2k\pi](k\in\mathbf{Z})$

#### 单调递减

$[\frac\pi{2}+2k\pi, \frac{3\pi}2+2\pi](k\in\mathbf{Z})$

### 余弦函数

#### 单调递增

$[2k\pi-\pi, 2k\pi](k\in\mathbf{Z})$

#### 单调递减

$[2k\pi, 2k\pi+\pi](k\in\mathbf{Z})$

### 正切函数

#### 单调递增

$(-\frac\pi{2}+k\pi,\frac\pi{2}+k\pi)(k\in\mathbf{Z})$

## 奇偶性

#### 偶函数

$f(x)=f(-x)$

#### 奇函数

$-f(x)=f(-x)$

### 正弦函数

$\sin(-x)=-\sin(x)$

### 余弦函数

$\cos(-x)=\cos{x}$

### 正切函数

$\tan(-x)=-\tan{x}$

## 三角恒等变换

### 同名两角和

#### 正弦函数

$\sin(\alpha+\beta)=\sin\alpha\cos\beta+\sin\beta\cos\alpha$

$(S_{(\alpha+\beta)})$

#### 余弦函数 {#cosplus}

$\cos(\alpha+\beta)=\cos\alpha\cos\beta-\sin\alpha\sin\beta$

$(C_{(\alpha+\beta)})$

#### 正切函数

$\tan(\alpha+\beta)=\frac{\tan\alpha+\tan\beta}{1-\tan\alpha\tan\beta}$

$(T_{(\alpha+\beta)})$

### 同名两角差

#### 正弦函数

$\sin(\alpha-\beta)=\sin\alpha\cos\beta-\sin\beta\cos\alpha$

$(S_{(\alpha-\beta)})$

#### 余弦函数

$\cos(\alpha-\beta)=\cos\alpha\cos\beta+\sin\alpha\sin\beta$

$(C_{(\alpha-\beta)})$

#### 正切函数

$\tan(\alpha-\beta)=\frac{\tan\alpha-\tan\beta}{1+\tan\alpha\tan\beta}$

$(T_{(\alpha-\beta)})$

### 同名倍角

#### 正弦函数

$\sin2\alpha=2\sin\alpha\cos\beta$

#### 余弦函数

$\cos2\alpha=\cos^2\alpha-\sin^2\beta$

$\cos2\alpha=1-2\sin^2\alpha$

$\cos2\alpha=2\cos^2\alpha-1$

#### 正切函数

$\tan2\alpha=\frac{2\tan\alpha}{1-\tan^2\alpha}$

### 同名半角

#### 正弦函数

$\sin\frac12\alpha=\sqrt{\frac{1-\cos\alpha}2}$

#### 余弦函数

$\cos\frac12\alpha=\sqrt{\frac{1+\cos\alpha}2}$

#### 正切函数

$\tan\frac12\alpha=\sqrt{\frac{1-\cos\alpha}{1+\cos\alpha}}$

### 积化和差

#### 正弦函数

$\sin\alpha\sin\beta=-\frac{1}2[\cos(\alpha+\beta)-\cos(\alpha-\beta)]$[^howtosin]

[^howtosin]: $\cos(\alpha+\beta)-\cos(\alpha-\beta)$，参见[同名余弦函数两角和](#cosplus)

#### 余弦函数

$\cos\alpha\cos\beta=-\frac{1}2[\cos(\alpha+\beta)+\cos(\alpha-\beta)]$

#### 正余弦混合

$\sin\alpha\cos\beta=\frac{1}2[\sin(\alpha+\beta)+\sin(\alpha-\beta)]$

### 和差化积

#### 正弦函数

$\sin\alpha+\sin\beta=2\sin\frac{\alpha+\beta}2\cos\frac{\alpha-\beta}2$[^howsin]

[^howsin]: $\sin\alpha=\sin(\frac{\alpha+\beta}2+\frac{\alpha-\beta}2)=\sin\frac{\alpha+\beta}2\cos\frac{\alpha-\beta}2+\sin\frac{\alpha-\beta}2\cos\frac{\alpha+\beta}2$

$\sin\alpha-\sin\beta=2\cos\frac{\alpha+\beta}2\sin\frac{\alpha-\beta}2$

#### 余弦函数

$\cos\alpha+\cos\beta=2\cos\frac{\alpha+\beta}2\cos\frac{\alpha-\beta}2$

$\cos\alpha-\cos\beta=-2\sin\frac{\alpha+\beta}2\sin\frac{\alpha-\beta}2$

#### 正切函数

$\tan\alpha+\tan\beta=\frac{\sin(\alpha+\beta)}{\cos\alpha\cos\beta}$[^howtan]

[^howtan]: $\tan\alpha=\frac{\sin\alpha}{\cos\alpha}$

$\tan\alpha-\tan\beta=\frac{\sin(\alpha-\beta)}{\cos\alpha\cos\beta}$

## $y=A\sin(\omega{x}+\phi)$

周期：

$T=\frac{2\pi}{|\omega|}$

定义域：
$\mathbf{R}$

值域：

$[-|A|, |A|]$

对称轴：

直线 $x=\frac{2k\pi+\pi-2\phi}{2\omega}(k\in\mathbf{Z})$

对称中心：

$(\frac{2k\pi-\phi}\omega, 0)(k\in\mathbf{Z})$

零点：

$\frac{2k\pi-\phi}\omega(k\in\mathbf{Z})$

极值点：

$(\frac{4k\pi-\pi-2\phi}{2\omega}, A)(k\in\mathbf{Z})$

$(\frac{4k\pi+\pi-2\phi}{2\omega}, -A)(k\in\mathbf{Z})$

<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP1%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E6%A0%B8%E5%BF%83%E7%82%B9%E7%B3%BB%E7%BB%9F%E5%A4%8D%E4%B9%A0%E2%80%94%E2%80%94%E5%87%BD%E6%95%B0%E7%AF%87%20%20%20.%E6%9C%80%E5%90%8E%E5%85%AD%E8%AF%BE%E7%AC%AC1%E8%AF%BE.mp4?Authorization=3_20220703021000_22fd034276577c21a65c0ad3_d89a15fc1231bea961e1e0b2b7c3ff3f0deb8e07_004_20220710021000_0000_dnld" type="video/mp4">
</video>
<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%9F%A5%E8%AF%86%E6%96%B9%E6%B3%95%E5%BF%AB%E9%80%9F%E6%A2%B3%E7%90%86/%5BP2%5D2022%E9%AB%98%E8%80%83%E6%95%B0%E5%AD%A6%21%E6%A0%B8%E5%BF%83%E7%82%B9%E7%B3%BB%E7%BB%9F%E5%A4%8D%E4%B9%A0%E2%80%94%E2%80%94%E4%B8%89%E8%A7%92%E7%AF%87%20%20%20.%E6%9C%80%E5%90%8E%E5%85%AD%E8%AF%BE%E7%AC%AC2%E8%AF%BE.mp4?Authorization=3_20220703072257_b131008ec06005e05362789d_469e42d9a0b9f21f1b6844514a197acfdff5a008_004_20220710072257_0000_dnld" type="video/mp4">
</video>
