# 对数

## 定义

$x=\log_aN$

其中，$a$是底数，$N$是真数

当$a>0, a≠1$时，$a^x=N\Leftrightarrow{x}=\log_aN$，可得：

- 负数和零没有对数

- $\log_a1=0\\\log_aa=1$

自然对数：

$e=2.71828\cdots$

## 运算性质

$\log_a(MN)=\log_aM+\log_aN$

$\log_a\frac{M}{N}=\log_aM-\log_aN$

$\log_aM^n=n\log_aM(n\in{R})$

对数换底公式：

$\log_ab=\frac{\log_cb}{\log_ca}(a>0, a≠1; b>0; c>0, c≠1)$

## 对数函数

$y=\log_ax(a>0, a≠0)$

定义域：$(0,+\infty)$

值域：$\mathbf{R}$

单调性：

- $0<x<1\Rightarrow$ 单调递减
- $a>1\Rightarrow$ 单调递增

### 反函数

定义域与值域互换的两个函数，即：

$y=a^x(a>0, a≠1)\Leftrightarrow{y}=log_ax(a>0, a≠1)$

### 函数零点

方程 $f(x)=0$ 有实数解 $\Leftrightarrow$ 函数$y=f(x)$有零点 $\Leftrightarrow$ 函数 $y=f(x)$ 的图像与 $x$ 轴有公共点

#### 零点存在定理

如果函数 $y=f(x)$ 在区间 $[a, b]$ 上的图像是一条连续不断的曲线，且有 $f(a)f(b)<0$ ，那么，函数 $y=f(x)$ 在区间 $(a, b)$ 内至少有一个零点，即存在 $c\in(a,b)$ ，使得 $f(c)=0$ ，
这个 $c$ 也就是方程 $f(x)=0$ 的解.
