# 向导

如果你想要参与本项目的编写，你需要懂得以下几点。

## Github

本项目托管在Github上，你可能需要了解一些科学的上网方法。

## Git

Git是一个版本管理系统，它在命令行下有点不好理解，但是我们可以使用诸如VS Code之类的图形化软件来方便使用。

## VS Code

VS Code是一个代码编辑器，它具有丰富的插件库，众多强大的插件方便了我们进行文档的编写。

推荐以下插件：

!!! Note "Markdown Image"

    ID: hancel.markdown-image

    说明: Easy to insert a image to markdown

    版本: 1.1.22

    发布者: Hancel.Lin

    VS Marketplace 链接: https://marketplace.visualstudio.com/items?itemName=hancel.markdown-image

!!! Note "Markdown Math"

    ID: koehlma.markdown-math

    说明: Adds math support to VS Code's built-in markdown preview.

    版本: 0.1.0

    发布者: Maximilian Köhl

    VS Marketplace 链接: https://marketplace.visualstudio.com/items?itemName=koehlma.markdown-math

!!! Note "Markdown Preview Enhanced"

    ID: shd101wyy.markdown-preview-enhanced

    说明: Markdown Preview Enhanced ported to vscode

    版本: 0.6.3

    发布者: Yiyi Wang
    
    VS Marketplace 链接: https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced

## Mkdocs

MKdocs是适用于Markdown的文档网页生成器，文档源码必须符合Mkdocs的规范。

本项目支持以下拓展语法：

- 检查框
- 删除线
- 警告框
- 缩进列表