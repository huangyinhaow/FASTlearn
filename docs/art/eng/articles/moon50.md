# 肯尼迪登月演说

But why, some say, the moon? 

Why choose this as our goal? 

And they may well ask why climb the highest mountain? 

Why, 35 years ago, fly the Atlantic? 

Why does Rice play Texas?

We choose to go to the moon. 

We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard, because that goal will serve to organize and measure the best of our energies and skills, because that challenge is one that we are willing to accept, one we are unwilling to postpone, and one which we intend to win, and the others, too.

<style>
.md-typeset video {
  max-width: 100%;
}
</style>

<video controls>
      <source id="mp4" src="https://d1.xf-yun.cn/file/3hyhhyhhyh/%E4%BA%BA%E7%B1%BB%E7%99%BB%E6%9C%8850%E5%B9%B4%E3%80%90%E5%AD%97%E5%B9%95%E7%89%88%E3%80%91%E8%82%AF%E5%B0%BC%E8%BF%AA%E7%99%BB%E6%9C%88%E6%BC%94%E8%AE%B2%E2%80%9C%E6%88%91%E4%BB%AC%E9%80%89%E6%8B%A9%E7%99%BB%E6%9C%88+We+Choose+to+Go+to+the+Moon%E2%80%9D.mp4" type="video/mp4">
</video>
